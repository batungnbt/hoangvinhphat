<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryController extends CI_Controller {

    public function index()
	{
    return $this->load->view('admin/categories/index');
	}

	public function create()
    {

    }

    public function store()
    {

    }

    public function edit($id)
    {
    return $this->load->view('admin/categories/edit');
    }

    public function update()
    {

    }

    public function destroy()
    {

    }



}

