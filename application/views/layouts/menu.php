<nav class="menu navbar navbar-expand-lg navbar-light  bg-color "   >

    <div class="menu-icon navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onclick="myFunction(this)">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
    </div>

    <div class="collapse navbar-collapse  py-2" id="navbarSupportedContent" >
        <ul class="navbar-nav mr-auto mx-auto">
            <li class="nav-item bg-li">
                <a class="nav-link " href="" ><span> <i class="fas fa-home" style="padding-right: 4px"></i>TRANG CHỦ</span></a>
            </li>
            <li class="nav-item bg-li">
                <a class="nav-link  " href="gioi-thieu"><span class="">GIỚI THIỆU</span></a>
            </li>
            <li class="nav-item dropdown bg-li">
                <a class="nav-link dropdown-toggle " href="chong-tham" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span>CHỐNG THẤM</span>
                </a>
                <ul class="dropdown-menu drop-menu " aria-labelledby="navbarDropdown">
                    <li>
                        <a class="dropdown-item  py-2" href="chong-tham-ban-cong.html">Chống thấm ban công</a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="chong-tham-san-mai.html">Chống thấm sàn mái</a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="chong-tham-san-mai-be-thong.html">Chống thấm sàn bê tông </a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="chong-tham-ho-boi.html">Chống thấm hồ bơi</a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="chong-tham-xu-ly-cac-vet-nut.html">Chống thấm xử lý các vết nứt</a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="dong-ton-chong-tham.html">Đóng tôn chống thấm</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown bg-li">
                <a class="nav-link dropdown-toggle " href="thong-tat" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span>THÔNG TẮT</span>
                </a>
                <ul class="dropdown-menu drop-menu " aria-labelledby="navbarDropdown">
                    <li>
                        <a class="dropdown-item  py-2" href="thong-tat-san-toilet.html">Thông tắt sàn toilet </a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="thong-tat-bon-cau-toilet.html">Thông tắt bồn cầu toilet </a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="thong-tat-bon-cau-rua-chen.html">Thông tắt bồn cầu rửa chén </a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="thong-tat-lavobo-bon-chau.html">Thông tắt Lavobo bồn chậu </a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="thong-tat-rut-ham-cau.html">Thông tắt rút hầm cầu </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown bg-li">
                <a class="nav-link dropdown-toggle " href="sua-chua" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span>SỬA CHỮA</span>
                </a>
                <ul class="dropdown-menu drop-menu " aria-labelledby="navbarDropdown">
                    <li>
                        <a class="dropdown-item  py-2" href="sua-chua-dien-nuoc.html">Sửa chữa điện nước</a>
                    </li>
                    <li>
                        <a class="dropdown-item  py-2" href="sua-chua-nha-o.html">Sửa chữa nhà ở</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item bg-li">
                <a class="nav-link" href="tin-tuc"><span>TIN TỨC</span></a>
            </li>
            <li class="nav-item bg-li">
                <a class="nav-link " href="tuyen-dung"><span>TUYỂN DỤNG</span></a>
            </li>
            <li class="nav-item bg-li">
                <a class="nav-link " href="lien-he"><span>LIÊN HỆ</span></a>
            </li>

        </ul>

    </div> <!-- MENU --> <!-- MENU -->
</nav> <!-- END MENU -->