<div class="container-fluid">   <!-- INFOR -->
    <div class="row info">
        <div class="col-md-12">
            <div class="row py-2">
                <div class="col-md-3 ">
                    <i class="fas fa-map-marker-alt"></i>
                    <span>177 Âu Cơ,Liên Chiểu,Hòa Khánh</span>
                </div>
                <div class="col-md-3">
                    <i class="fas fa-envelope"></i>
                    <span>hoangvinhphat@gmail.com</span>
                </div>
                <div class="col-md-4 ">
                    <i class="fas fa-phone"></i>
                    <span>0905979937 - 0963030949</span>
                </div>
                <div class="col-md-2 social text-center">
                    <a href="https://www.facebook.com/messages/t/vinh.ngo.71216" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-youtube-play"></i></a>
                </div>
            </div>
        </div>


    </div>
</div>  <!-- END INFOR -->