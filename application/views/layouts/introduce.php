<div class="col-md-12 gioithieu "> <!-- INTRODUCE -->
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="title-heading-introduce">
                    <h3><i class="fa fa-building-o" aria-hidden="true"></i>   GIỚI THIỆU CÔNG TY</h3>
                </div>
                <div class="introduce-left">
                    <div class="introduce-left-img d-block d-md-none">
                        <a href="#">
                            <img  src="<?php echo base_url() ?>resources/images/banner-gioithieu.png" alt="mia duong" width="100%">
                        </a>
                    </div>
                    <h3><a href="#">CÔNG TY CHỐNG THẤM HOÀNG VINH PHÁT</a></h3>
                    <p>Liên hệ : 0905979937 - 0963030949</p>
                    <p>Chúng tôi công ty chống thấm tại ĐÀ NẴNG cùng đội ngũ chuyên viên, giám sát, kỹ thuật viên thi công lành nghề được đào tạo từ thực tế đã trải nghiệm trên nhiều dự án xây dựng, và chống thấm kết cấu khắp các vùng miền cả nước. </p>
                    <a href="#" class="btn-detail">Xem chi tiết</a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="introduce-right">
                    <div class="introduce-right-img">
                        <a href="#">
                            <img class="img-thumbnail" src="<?php echo base_url() ?>resources/images/banner-gioithieu.png" alt="mia duong" width="100%">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- END INTRODUCE -->