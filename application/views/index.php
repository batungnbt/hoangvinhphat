<!DOCTYPE html>
<html lang="en"><head>
    <title> Chống thấm Hoàng Vinh Phát </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="<?php echo base_url()?>resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url()?>resources/fonts/css/all.css">
    <link rel="stylesheet" href="<?php echo base_url()?>resources/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:500" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">


    <style>
        /* Make the image fully responsive */
        body,html{
            background-color:#F2F6F8;
        }
        @font-face {
            font-family: 'UTM-American-Sans';
            src:url("<?php echo base_url()?>resources/fonts/UTM American Sans.ttf");
            font-weight: normal;
            font-style: normal;
        }


    </style>
</head>
<body >

<?php $this->load->view('layouts/info') ?>

<?php $this->load->view('layouts/banner') ?>

<?php $this->load->view('layouts/menu') ?>

<?php $this->load->view('layouts/introduce') ?>

<div class="container-fluid mt-1"> <!-- CONTENT -->
    <div class="row">

        <?php $this->load->view('layouts/content_left') ?>

        <div class="col-md-9 content-right mt-3"> <!-- CONTENT RIGHT -->
            <div class="title">
                <h3><a href="chong-tham.html">DỊCH VỤ CHỐNG THẤM</a></h3>
            </div>
            <div class="row mt-4  ">
                <div class="col-md-4 mt-2  ">
                    <h4 class="title-chongtham"><a href="#" >Chống thấm ban công</a></h4>
                    <div class="card" style="width: 100%;">
                        <a href="#"><img class="card-img-top" src="<?php echo base_url() ?>resources/images/chong-tham-ban-cong.jpg" alt="Card image cap"></a>
                        <div class="card-body">
                            <p class="card-text">Chống thấm ban công sử dụng dung dịch Water Seal là một trong những công nghệ chống thấm mới hiện nay</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-2">
                    <h4 class="title-chongtham"><a href="#" >Chống thấm sàn mái</a></h4>
                    <div class="card" style="width: 100%;">
                        <a href="#"><img class="card-img-top" src="<?php echo base_url() ?>resources/images/chong-tham-san-mai.jpg"></a>
                        <div class="card-body">
                            <p class="card-text">Kỹ thuật thi công chống thấm sàn mái sử dụng hai phương pháp chong tham hiệu quả và thông dụng nhất</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-2">
                    <h4 class="title-chongtham"><a href="#" >Chống thấm sàn bê tông</a></h4>
                    <div class="card" style="width: 100%;">
                        <a href="#"><img class="card-img-top" src="<?php echo base_url() ?>resources/images/chong-tham-ban-cong.jpg"></a>
                        <div class="card-body">
                            <p class="card-text">Sàn mái bê tông bị thấm dột không phải là ván đề quá hiếm gặp tại các công trình xây dựng ở Việt Nam</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-2">
                    <h4 class="title-chongtham"><a href="#" >Chống thấm hồ bơi</a></h4>
                    <div class="card" style="width: 100%;">
                        <a href="#"><img class="card-img-top" src="<?php echo base_url() ?>resources/images/chong-tham-ho-boi.jpg"></a>
                        <div class="card-body">
                            <p class="card-text">Bể bơi, hồ bơi, bể nước dùng trong 1 thời gian không thể tránh khỏi hiện tượng rò rỉ</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-2">
                    <h4 class="title-chongtham"><a href="#" >Chống thấm xử lý vết nứt</a></h4>
                    <div class="card" style="width: 100%;">
                        <a href="#"><img class="card-img-top" src="<?php echo base_url() ?>resources/images/xu-ly-vet-nut.jpg" ></a>
                        <div class="card-body">
                            <p class="card-text">Các công trình xây dựng sẽ ngày càng xuống cấp do phải hứng chịu nhiều tác động của thời tiết</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-2">
                    <h4 class="title-chongtham"><a href="#" >Đóng tôn chống thấm</a></h4>
                    <div class="card" style="width: 100%;">
                        <a href="#"><img class="card-img-top" src="<?php echo base_url() ?>resources/images/dong-ton-chong-tham.jpg"></a>
                        <div class="card-body">
                            <p class="card-text">Bạn không còn phải lo lắng vì tường bị ẩm mốc hay những vết sơn đã bị ố màu, những vết vữa rời </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="title mt-4">
                <h3><a href="chong-tham.html">TIN TỨC NỔI BẬT</a></h3>
            </div>
            <div class="row mt-3">
                <div class="col-md-6 tintuc ">
                    <h4 class="title-tintuc"><a href="">Ngăn nước, giảm nhiệt ngôi nhà bằng vật liệu chống thấm mái bê tông</a></h4>
                    <div class="img-tintuc">
                        <a href="">
                            <img src="<?php echo base_url() ?>resources/images/tin-tuc-1.jpg" alt="" style="width: 100%">
                        </a>
                    </div>
                    <div class="mota-tintuc">
                        nắng nóng kỷ lục luôn là nỗi kinh hoàng của nhiều gia đình hiện nay. Không chỉ ảnh hưởng tới cuộc sống sinh hoạt
                    </div>

                </div>
                <div class="col-md-6 tintuc ">
                    <h4 class="title-tintuc"><a href="">Phương pháp rửa nhà vệ sinh luôn bóng loáng</a></h4>
                    <div class="img-tintuc">
                        <a href="">
                            <img src="<?php echo base_url() ?>resources/images/tin-tuc-2.jpg" alt="" style="width: 100%">
                        </a>
                    </div>
                    <div class="mota-tintuc">
                        Vệ sinh nhà tắm luôn như mới không phải ai cũng biết, hôm nay dichvuhangngay sẽ giúp bạn đọc sao cho nhà vệ sinh luôn sạch bóng nhất,...
                    </div>

                </div>

            </div>
            <div class="row mt-3">
                <div class="col-md-6 tintuc">
                    <h4 class="title-tintuc"><a href="">Xử lý trần thạch cao bị ngấm nước như thế nào?</a></h4>
                    <div class="img-tintuc">
                        <a href="">
                            <img src="<?php echo base_url() ?>resources/images/tin-tuc-3.jpg" alt="" style="width: 100%">
                        </a>
                    </div>
                    <div class="mota-tintuc">
                        Ngày nay các thiết kế được ứng dụng trần thạch cao đã và đang trở nên khá phổ biến trong đời sống...
                    </div>

                </div>
                <div class="col-md-6 tintuc">
                    <h4 class="title-tintuc"><a href="">3 yếu tố then chốt trong phong thủy nhà ở hút tài lộc vào nhà</a></h4>
                    <div class="img-tintuc">
                        <a href="">
                            <img src="<?php echo base_url() ?>resources/images/tin-tuc-4.jpg" alt="" style="width: 100%">
                        </a>
                    </div>
                    <div class="mota-tintuc">
                        nắng nóng kỷ lục luôn là nỗi kinh hoàng của nhiều gia đình hiện nay. Không chỉ ảnh hưởng tới cuộc sống sinh hoạt
                    </div>

                </div>

            </div>
        </div>



    </div> <!-- END CONTENT RIGHT -->


</div>

</div> <!--END CONTENT -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 footer text-center">
            <i class="fas fa-phone-volume"></i>   Hotline: 0905979937 - 0963030949<br>
            <i class="far fa-envelope"></i>    chongthamhoangvinhphat.com<br>
            <i class="fas fa-map-marker-alt"></i>   177 Âu Cơ,Liên Chiểu,Hòa Khánh<br>
            <a href="#">Thi công chống thấm |</a>
            <a href="#">Thông tắt |</a>
            <a href="#">Sửa chữa nhà ở</a>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>resources/js/jquery-slim.min.js" ></script>
<script src="<?php echo base_url()?>resources/js/bootstrap.min.js"></script>
<script>
    function myFunction(x) {
        x.classList.toggle("change");
    }
</script>

</body>
</html>

